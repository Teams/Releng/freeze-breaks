# Approving/Rejecting Freeze Breaks

Please review the [freeze break request guidelines.](https://handbook.gnome.org/release-planning/freezes.html)

Please note string freeze breaks follow a [separate process](https://handbook.gnome.org/release-planning/freezes.html#string-freeze) and are not handled here. You might need to request both a UI freeze break and a string freeze break to land a change.

To avoid delays, please make life easier by:

 * Let us know that the module's maintainer has approved the patch on the
   condition that we approve it (if they haven't yet, make sure to get their
   approval first).
 * Clearly tell us if it is an API break, a UI change, a feature change, a
   string change, several categories of these, etc..
 * Briefly explain what the bug being fixed is and its impact (how many people
   will be affected, how will they be affected, etc.)
 * Tell us why you think the change should not wait for the next development
   phase (unless already covered in explaining the bug impact).
 * Let us know of anything you've done to make the freeze break safer (why the
   merge request might not be as scary as it looks, how hard would it be to
   revert if it turned out to be necessary to do so, who else has reviewed the
   merge request, who has tested it, how much testing was done, etc.)
 * If you want to break the hard code freeze, then please persuade us that the
   merge request is very safe or has been very well tested (or that the
   situation is bad enough currently that it couldn't really get worse with a
   new patch, e.g. serious crashes).
